from ase import Atoms
import os
import warnings
warnings.filterwarnings("ignore")
from pathlib import Path
#ASE terms
from ase import Atoms
from ase.db import connect
from ase.io import read,write, Trajectory
from ase.optimize import BFGS as BFGS
from ase.calculators.aims import Aims,AimsProfile
from ase.build import molecule,bulk
import pytest
from ase import Atoms
from ase.calculators.aims import AimsCube
from ase.optimize import QuasiNewton
from pathlib import Path

def pbe_relax_calc():
    parameters={'xc':'pbe',
    'sc_accuracy_rho':'1E-5',
    'sc_accuracy_eev':'1E-3',
    'sc_accuracy_etot':'1E-6',
    'sc_iter_limit':"1000",
    'relativistic':"atomic_zora scalar",
    'adjust_scf':'always 1',
    'default_initial_moment':'hund',
    'species_dir':"/u/akhils/softwares/aims_221103/species_defaults/defaults_2020/tight"}
    return parameters

def hse_scf_calc():
    parameters={'xc':'hse06 0.11',
    'hse_unit':'bohr-1',
    'hybrid_xc_coeff':'0.25',
    'sc_accuracy_rho':'1E-5',
    'sc_accuracy_eev':'1E-3',
    'sc_accuracy_etot':'1E-6',
    'sc_iter_limit':"1000",
    'relativistic':"atomic_zora scalar",
    'spin':'collinear',
    'adjust_scf':'always 1',
    'default_initial_moment':'hund',
    'species_dir':"/u/akhils/softwares/aims_221103/species_defaults/defaults_2020/tight"}
    return parameters

#Task 1 PBE relaxation
def pbe_relax(atoms,spin=None):
    parameters=pbe_relax_calc()
    if spin is not None:
        parameters[spin]='collinear' 
    profile=AimsProfile('srun aims.221103.scalapack.mpi.x')
    aims_calc=Aims(profile=profile,**parameters)
    atoms.calc=aims_calc
    energy=atoms.get_potential_energy()
    return energy 

#Task 2 HSE SCF calculation
def hse_scf(atoms,spin=None):
    parameters=hse_scf_calc()
    if spin is not None:
        parameters[spin]='collinear'
    profile=AimsProfile('srun aims.221103.scalapack.mpi.x')
    aims_calc=Aims(profile=profile,**parameters)
    atoms.calc=aims_calc
    energy=atoms.get_potential_energy()
    return energy
