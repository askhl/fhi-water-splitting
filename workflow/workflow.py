import asr


@asr.workflow
class DummyWorkflow:
    atoms = asr.var()

    @asr.task
    def pbe_relaxation(self):
        """
        PBE relaxation : Relax geometry with PBE
        """
        return asr.node('pbe_relax',#dummytasks.function1',
        atoms=self.atoms)

    @asr.task
    def HSE_scf_calculation(self):
        """
        SCF: SCF calculation taking path from pbe_relax as input
        """
        return asr.node('hse_scf',  #dummytasks.function1',
                        atoms=self.atoms)

@asr.parametrize_glob('*/material')
def workflow(material):
    return DummyWorkflow(atoms=material)
