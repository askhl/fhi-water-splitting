import pandas as pd
import os, sys, re
from mp_api.client import MPRester as mpr
from pymatgen.core.composition import Composition
from pymatgen.analysis.phase_diagram import PhaseDiagram, PDPlotter
from pymatgen.analysis.pourbaix_diagram import PourbaixDiagram, PourbaixPlotter
from pymatgen.io.ase import AseAtomsAdaptor
from ase.db import connect
from pymatgen.ext.matproj import MPRester as leg_mpr

# Set API keys for Materials Project and legCSD
api_key = "nEI6oYidbkS4K4K7mEBEIoIVFoXe9DKA"
mpr = mpr(api_key)
leg_api_key = "TImMWwVkb3Hifdho"
leg_mpr = leg_mpr(leg_api_key)

# Set working directory and connect to oxide database
work_dir = os.getcwd()
oxide_db = connect('oxides.db')

# split oxide formula into elements
def split_oxide_formula(oxide_formula, O_include=None):
    element_regex = r'[A-Z][a-z]?'
    elements = re.findall(element_regex, oxide_formula)
    if O_include == 'no':
        new_elements = [el for el in elements if el != 'O']
        return new_elements
    else:
        return elements

# get unique compositions from a list of oxide formulas
def get_unique_compositions(data):
    unique_compositions = []
    existing_ratios = set()
    for comp in data:
        composition = Composition(comp)
        elements = set([el.symbol for el in composition.elements])
        ratio = ":".join(sorted(elements))
        if ratio not in existing_ratios:
            unique_compositions.append(comp)
            existing_ratios.add(ratio)
    return unique_compositions

# get stable phase entries for an oxide
def get_phase_entries(oxide):
    exl_list = ['O2', oxide]
    comp = Composition(oxide)
    elements = [str(el) for el in comp.elements]
    entries = mpr.get_entries_in_chemsys(elements)
    pd = PhaseDiagram(entries)
    stable_entries = pd.stable_entries
    phase_list = [entry.composition.reduced_formula for entry in stable_entries if entry.composition.reduced_formula not in exl_list]
    return (stable_entries, phase_list)

# get stable Pourbaix entries for an oxide
def get_pourbaix_entries(oxide):
    exl_list = ['O2', oxide]
    comp = Composition(oxide)
    elements = [str(el) for el in comp.elements]
    exl_list += [el for el in elements if 'O' not in el]
    entries = leg_mpr.get_pourbaix_entries(elements)
    pbx = PourbaixDiagram(entries)
    stable_entries = pbx.stable_entries
    phase_list = [entry.composition.reduced_formula for entry in stable_entries if entry.phase_type == 'Solid' and entry not in exl_list]
    return stable_entries, phase_list
# Retrieve oxide formulas from the database
oxide_list = []
for row in oxide_db.select():
    oxide_list.append(row.formula)

# Get unique compositions from the oxide list
unique_comps = get_unique_compositions(oxide_list)

# Write stable phase entries to the phase_refs.db database
with connect('phase_refs.db') as db:
    for comp in unique_comps:
        phases = get_phase_entries(comp)[0]
        for phase in phases:
            pymat_structure = phase.structure
            ase_structure = AseAtomsAdaptor.get_atoms(pymat_structure)
            db.write(ase_structure)

# Write stable Pourbaix entries to the pourbaix_refs.db database,
#a good fraction of entries would be ions for which structures cannot be retrived
with connect('pourbaix_refs.db') as db:
    for comp in unique_comps:
        entries = get_pourbaix_entries(comp)[0]
        for entry in entries:
            mat_id = entry.entry_id
            if 'ion' not in mat_id:
                try:
                    structure = mpr.get_structure_by_material_id(mat_id)
                    ase_structure = AseAtomsAdaptor.get_atoms(structure)
                    db.write(ase_structure)
                except TypeError:
                    print(f"Not retrieved: {mat_id}, {entry.composition}")

